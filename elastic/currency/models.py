# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Currency package"


class Currency(models.Model):
    """
    Валюты
    """
    class Meta:
        verbose_name = _("Currency")
        verbose_name_plural = _("Currencies")

    #: Код валюты
    code = models.PositiveIntegerField(verbose_name=_('Code'))

    #: Наименование валюты
    name = models.CharField(max_length=128, verbose_name=_('Name'))

    #: Символьное обозначение валюты
    symbol = models.CharField(max_length=5, verbose_name=_('Symbol'))

    def __unicode__(self):
        return u"{0} - {1}".format(self.code, self.name)


class Rate(models.Model):
    """
    Курсы валюты
    """
    class Meta:
        verbose_name = _("Currency rate")
        verbose_name_plural = _("Currency rates")

    TYPE_RATE = (
        ('INT', _('Internal')),  # Постоянное число — внутренний курс, который вы используете.
        ('CBRF', _('CBRF')),   # Курс по Центральному банку РФ.
        ('NBU', _('NBU')),     # Курс по Национальному банку Украины.
        ('NBK', _('NBK')),     # Курс по Национальному банку Казахстана.
        ('СВ', _('CB')),       # Курс по Центральному банку страны нахождения
    )

    #: Валюта
    currency = models.OneToOneField(Currency, verbose_name=_('Currency'))

    #: Внутренний курс. Учитывается, если тип курса INT
    internal_rate = models.DecimalField(
        verbose_name=_('Internal rate'),
        max_digits=8, decimal_places=2,
        default=1.00
    )

    #: Курс центробанка. Учитывается, если тип курса не INT
    rate = models.DecimalField(
        verbose_name=_('Rate'),
        max_digits=8, decimal_places=2,
        default=1.00
    )

    #: Тип курса
    type_rate = models.CharField(max_length=5, choices=TYPE_RATE, verbose_name=_('Type rate'))

    #: "Надбавка" к курсу центробанка в процентах
    plus = models.PositiveIntegerField(default=0, verbose_name=_('Extra percent'))

    #: Вычисленное значение курса. Именно оно используется при конвертации
    real_rate = models.DecimalField(
        verbose_name=_('Real rate'),
        max_digits=8, decimal_places=2,
        default=0.00
    )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.type_rate is 'INT':
            self.real_rate = self.internal_rate
        else:
            self.real_rate = self.rate.real + self.rate.real / 100 * self.plus
        super(Rate, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return unicode(self.currency)
