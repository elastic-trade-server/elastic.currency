# -*- coding: utf-8 -*-


from django.contrib import admin
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Currency package"


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'symbol',)
    list_display_links = ('name',)

admin.site.register(Currency, CurrencyAdmin)


class RateAdmin(admin.ModelAdmin):
    list_display = ('currency', 'type_rate', 'real_rate',)
    list_display_links = ('currency',)

    readonly_fields = ('real_rate',)

admin.site.register(Rate, RateAdmin)
