# -*- coding: utf-8 -*-

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Currency package"


from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ElasticCurrency(AppConfig):
    name = 'elastic.currency'
    label = "currency"
    verbose_name = _('Currency')
