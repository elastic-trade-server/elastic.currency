# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.PositiveIntegerField(verbose_name='Code')),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('symbol', models.CharField(max_length=5, verbose_name='Symbol')),
            ],
            options={
                'verbose_name': 'Currency',
                'verbose_name_plural': 'Currencies',
            },
        ),
        migrations.CreateModel(
            name='Rate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('internal_rate', models.DecimalField(default=1.0, verbose_name='Internal rate', max_digits=8, decimal_places=2)),
                ('rate', models.DecimalField(default=1.0, verbose_name='Rate', max_digits=8, decimal_places=2)),
                ('type_rate', models.CharField(max_length=5, verbose_name='Type rate', choices=[(b'INT', 'Internal'), (b'CBRF', 'CBRF'), (b'NBU', 'NBU'), (b'NBK', 'NBK'), (b'\xd0\xa1\xd0\x92', 'CB')])),
                ('plus', models.PositiveIntegerField(default=0, verbose_name='Extra percent')),
                ('real_rate', models.DecimalField(default=0.0, verbose_name='Real rate', max_digits=8, decimal_places=2)),
                ('currency', models.OneToOneField(verbose_name='Currency', to='currency.Currency')),
            ],
            options={
                'verbose_name': 'Currency rate',
                'verbose_name_plural': 'Currency rates',
            },
        ),
    ]
