# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL
from tastypie.bundle import Bundle
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import ReadOnlyAuthorization
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url
from tastypie.utils.urls import trailing_slash
import json as simplejson
from django.core.serializers import json
from tastypie.serializers import Serializer

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Currency package"


class DojoRefSerializer(Serializer):
    def to_json(self, data, options=None):
        options = options or {}

        data = self.to_simple(data, options)

        if 'objects' in data:
            data = data['objects']

        return simplejson.dumps(data, cls=json.DjangoJSONEncoder, sort_keys=True)


class CurrencyResource(ModelResource):
    class Meta:
        queryset = Currency.objects.all()
        resource_name = 'currency'
        authentication = SessionAuthentication()
        authorization = ReadOnlyAuthorization()
        serializer = DojoRefSerializer()
        allowed_methods = ['get', ]
        filtering = {
            "code": ALL,
            "name": ALL,
            "symbol": ALL,
        }

    code = fields.IntegerField(attribute='code', help_text=_('Currency code'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    symbol = fields.CharField(attribute='symbol', help_text=_('Symbol'))

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        # Лечим проблему с предачей от dojo, в качестве значения 'все записи', метасимвола '*'
        for key, item in filters.items():
            if item == [u'*'] or item == ['*'] or item == u'*' or item == '*' or item == '' or item == u'':
                filters.pop(key)
            elif item[-1] == u'*':
                filters.pop(key)
                filters[u"{0}{1}".format(key, '__istartswith')] = item[0:-1]

        return super(CurrencyResource, self).build_filters(filters)
