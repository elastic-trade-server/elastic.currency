from django.conf.urls import patterns, include

from tastypie.api import Api
v1_api = Api(api_name='v1')


from api import *
v1_api.register(CurrencyResource())

urlpatterns = patterns(
    '',
    url(r'^api/', include(v1_api.urls)),
)
